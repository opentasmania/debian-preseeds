# Debian Preseeds

## Getting started

These [debian preseeds](https://wiki.debian.org/DebianInstaller/Preseed) are created for general use, however it should be noted that locale information is set to Tasmania.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of
usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably
include in the README.

## Support

For errors in the preseeds, open a ticket. For errors in Debian Preseed itself, please contact Debian.

## Contributing

Will be accepting preseeds from others as requested. This is primarily so that a well known location can host a number
of preseed files and thus make repetitive net installs easier.

## Authors and acknowledgment

This project was initiated by Peter Lawler

## License

AGPL Licence

## Project status

Intermittent.
